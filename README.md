### ЗАДАЧА
https://docs.google.com/document/d/1g_aH0YG1Rk7MJeSXFJXZIobc0z9QuqZdeRv4jdEmJJg/edit

### ЗАПУСК
* Необходимо указать параметр MySQL ft_min_word_len=1 и перезапустить демон.

```
./artisan migrate --seed
nohup ./artisan serve &
```

### ЗАПРОСЫ
```
/good/by/id/{id} - Выдача товара по ID
/good/by/producer/{producer} - Выдача товаров по производителю/производителям
/good/by/title/{title} - Выдача товаров по вхождению подстроки в названии
/good/by/catalogue/{catalogue_id} - Выдача товаров по разделу (только раздел)
/good/by/catalogue/{catalogue_id}/{deep?} - Выдача товаров по разделу и вложенным разделам
```

### АВТОР
Шатров Алексей Сергеевич <mail@ashatrov.ru>