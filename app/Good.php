<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Good extends Model {
	/**
	* Связанная с моделью таблица.
	*
	* @var string
	*/
	protected $table = 'good' ;

	/**
	* Определяет необходимость отметок времени для модели.
	*
	* @var bool
	*/
	public $timestamps = false;

	/**
	* Коллекция по полю `exists`
	*
	* @param bool $exists - признак существования
	*
	* @return Illuminate\Database\Eloquent\Collection
	*/
	public static function getByExists( $exists = true ) {
		return self::where( 'exists' , $exists )->select( 'good.*' ) ;
	}

	/**
	* Обработка строки для FTS
	*
	* @param string $fts - строка для поиска
	*
	* @return string - строка для поиска в формате FTS
	*/
	protected static function parseFTS( $fts ) {
		return preg_replace_callback( '{\s+|\W+|\w+}us' , function( $matches ) {
			$match = &$matches[ 0 ] ;

			if ( preg_match( '{\s}us' , $match ) ) {
				return ' ' ;
			}

			if ( preg_match( '{\W}us' , $match ) ) {
				return '+"' . quotemeta( $match ) . '"' ;
			}

			if ( is_numeric( $match ) ) {
				return '+"' . quotemeta( $match ) . '"' ;
			}

			return '+' . quotemeta( $match ) . '*' ;
		} , trim( $fts ) ) ;
	}

	/**
	* Коллекция по полю `title`
	*
	* @param string $title - строка для поиска
	*
	* @return Illuminate\Database\Eloquent\Collection
	*/
	public static function getByTitle( $title ) {
		return self::getByExists( )->whereRaw( "
MATCH( `title` ) AGAINST( ? IN BOOLEAN MODE )
		" , [ self::parseFTS( $title ) ] ) ;
	}

	/**
	* Коллекция по полю `producer`
	*
	* @param string $producer - строка для поиска
	*
	* @return Illuminate\Database\Eloquent\Collection
	*/
	public static function getByProducer( $producer ) {
		return self::getByExists( )->whereRaw( "
MATCH( `producer` ) AGAINST( ? IN BOOLEAN MODE )
		" , [ self::parseFTS( $producer ) ] ) ;
	}

	/**
	* Коллекция по полю `id`
	*
	* @param integer $id - идентификатор
	*
	* @return Illuminate\Database\Eloquent\Model
	*/
	public static function getById( $id ) {
		return self::getByExists( )->where( 'id' , $id ) ;
	}

	/**
	* Коллекция по полю `id`
	*
	* @param integer $catalogue_id - идентификатор каталога
	* @param bool $deep - нужно ли искать в дочерних разделах
	*
	* @return Illuminate\Database\Eloquent\Collection
	*/
	public static function getByCatalogueId( $catalogue_id , $deep = false ) {
		if ( empty( $deep ) ) {
			return self::getByExists( )->where( 'catalogue_id' , $catalogue_id ) ;
		}

		return self::getByExists( )
			->join( 'catalogue' , 'good.catalogue_id' , '=' , 'catalogue.id' )
			->orWhereRaw(
				"
( MATCH( `catalogue`.`children` ) AGAINST( ? IN BOOLEAN MODE ) )
				" ,  [ self::parseFTS( $catalogue_id ) ]
			) ;
	}
}
