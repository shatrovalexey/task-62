<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Good ;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function getGoodByTitle( $title ) {
		return Good::getByTitle( $title )->get( ) ;
	}

	public function getGoodByProducer( $producer ) {
		return Good::getByProducer( $producer )->get( ) ;
	}

	public function getGoodById( $id ) {
		return Good::getById( $id )->get( ) ;
	}

	public function getGoodByCatalogueId( $catalogue_id , $deep = false ) {
		return Good::getByCatalogueId( $catalogue_id , $deep )->get( ) ;
	}
}