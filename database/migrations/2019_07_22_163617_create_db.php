<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up( ) {
	Schema::create( 'catalogue' , function( Blueprint $table ) {
		$table->bigInteger( 'id' )->unique( ) ;
		$table->bigInteger( 'parent_id' ) ;
		$table->longText( 'children' ) ;
		$table->string( 'title' ) ;

		$table->index( [ 'parent_id' ] ) ;
	} ) ;

        Schema::create( 'good' , function ( Blueprint $table ) {
		$table->bigIncrements( 'id' ) ;
		$table->string( 'title' )->notNull( ) ;
		$table->tinyInteger( 'exists' )->default( false )->notNull( ) ;
		$table->decimal( 'price' , 13 , 2 )->default( false )->notNull( ) ;
		$table->bigInteger( 'catalogue_id' )->notNull( ) ;
		$table->text( 'producer' )->notNull( ) ;

		$table->index( [ 'catalogue_id' ] ) ;
	} ) ;

	DB::statement( "
ALTER TABLE `catalogue`
	ADD FULLTEXT( `children` ) ;
	" ) ;

	DB::statement( "
ALTER TABLE `good`
	ADD FULLTEXT( `title` ) ;
	" ) ;

	DB::statement( "
ALTER TABLE `good`
	ADD FULLTEXT( `producer` ) ;
	" ) ;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down( ) {
        Schema::dropIfExists( 'good' ) ;
        Schema::dropIfExists( 'catalogue' ) ;
    }
}
