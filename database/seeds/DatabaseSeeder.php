<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run( ) {
	/**
	* @var integer $catalogue_max - максимальное количество узлов каталога
	* @var integer $catalogue_good_max - максимальное количество товаров в узле каталога
	* @var integer $catalogue_producer_max - максимальное количество производителей товара в товаре
	* @var integer $price_max - максимальная стоимость товара
	* @var integer $string_max - максимальная длинна строки
	* @var array $catalogues - список каталогов
	*/
	$catalogue_max = 1e6 ;
	$catalogue_good_max = 1e2 ;
	$catalogue_producer_max = 1e2 ;
	$price_max = 1e10 ;
	$string_max = 1e1 ;

	$catalogues = [ ] ;

	for ( $i = 1 ; $i <= $catalogue_max ; $i ++ ) {
		$catalogue_id = rand( 1 , $catalogue_max ) ;
		$catalogue_id_child = rand( 1 , $catalogue_max ) ;

		if ( empty( $catalogues[ $catalogue_id ] ) ) {
			$catalogues[ $catalogue_id ] = [
				'parent_id' => rand( 0 , $catalogue_max ) ,
				'children' => [ $catalogue_id , $catalogue_id_child ] ,
			] ;

			continue ;
		}

		$catalogues[ $catalogue_id ][ 'children' ][] = $catalogue_id_child ;
	}

	$catalogue_table = DB::table( 'catalogue' ) ;
	$good_table = DB::table( 'good' ) ;

	foreach ( $catalogues as $catalogue_id => &$catalogue ) {
		if ( $catalogue[ 'parent_id' ] && empty( $catalogues[ $catalogue[ 'parent_id' ] ] ) ) {
			unset( $catalogues[ $catalogue_id ] ) ;

			continue ;
		}

		$catalogue[ 'children' ] = array_unique( $catalogue[ 'children' ] ) ;
		$catalogue[ 'children' ] = array_filter( $catalogue[ 'children' ] , function( $catalogue_child_id ) use( &$catalogues ) {
			return isset( $catalogues[ $catalogue_child_id ] ) ;
		} ) ;

		$catalogue_table->insert( [
			'id' => $catalogue_id ,
			'parent_id' => $catalogue[ 'parent_id' ] ,
			'children' => implode( ',' , $catalogue[ 'children' ] ) ,
			'title' => str_random( $string_max ) ,
		] ) ;

		for ( $i = 0 ; $i < rand( 1 , $catalogue_good_max ) ; $i ++ ) {
			$producers = [ ] ;

			for ( $j = 0 ; $j < rand( 1 , $catalogue_producer_max ) ; $j ++ ) {
				$producers[] = str_random( $string_max ) ;
			}

			$producers = array_unique( $producers ) ;

			$good_table->insert( [
				'title' => str_random( $string_max ) ,
				'exists' => rand( 0 , 1 ) ,
				'price' => rand( 1 , $price_max ) ,
				'catalogue_id' => $catalogue_id ,
				'producer' => implode( ',' , $producers ) ,
			] ) ;
		}

		unset( $catalogues[ $catalogue_id ] ) ;
	}
    }
}
