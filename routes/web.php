<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get( '/good/by/id/{id}' , 'Controller@getGoodById' ) ;
Route::get( '/good/by/producer/{producer}' , 'Controller@getGoodByProducer' ) ;
Route::get( '/good/by/title/{title}' , 'Controller@getGoodByTitle' ) ;
Route::get( '/good/by/catalogue/{catalogue_id}/{deep?}' , 'Controller@getGoodByCatalogueId' ) ;

